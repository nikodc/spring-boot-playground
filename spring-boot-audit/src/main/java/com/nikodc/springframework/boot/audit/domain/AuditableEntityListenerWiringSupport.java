package com.nikodc.springframework.boot.audit.domain;

import com.nikodc.springframework.boot.audit.services.DateProvider;
import com.nikodc.springframework.boot.audit.services.UserProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by nikodc on 08/03/15.
 */
@Component
public class AuditableEntityListenerWiringSupport {

    private static AuditableEntityListenerWiringSupport INSTANCE;

    @Autowired
    private UserProvider userProvider;

    @Autowired
    private DateProvider dateProvider;

    @PostConstruct
    public void init() {
        INSTANCE = this;
    }

    public static AuditableEntityListenerWiringSupport getInstance() {
        return INSTANCE;
    }

    public void wire(AuditableEntityListener listener) {
        listener.wire(userProvider, dateProvider);
    }

}
