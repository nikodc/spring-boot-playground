package com.nikodc.springframework.boot.audit.services;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;

@Service
public class DateProvider {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Date getCurrentDate() {
        return (Date) entityManager.createQuery("select now() from Dual").getSingleResult();
    }

}
