package com.nikodc.springframework.boot.audit.domain;

import com.nikodc.springframework.boot.core.DomainEntity;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditableEntityListener.class)
public class Auditable extends DomainEntity {

    public Auditable() {
        super();
    }

    @Column(name = "audit_user", length = 50)
    protected String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "audit_date")
    protected Date createdDate;

    @Column(name = "modif_user", length = 50)
    protected String modifiedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modif_date")
    protected Date modifiedDate;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

}
