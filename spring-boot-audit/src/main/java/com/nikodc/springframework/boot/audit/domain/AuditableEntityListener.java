package com.nikodc.springframework.boot.audit.domain;

import com.nikodc.springframework.boot.audit.services.DateProvider;
import com.nikodc.springframework.boot.audit.services.UserProvider;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class AuditableEntityListener {

    private UserProvider userProvider;

    private DateProvider dateProvider;

    @PrePersist
    public void prePersist(Object object) {
        AuditableEntityListenerWiringSupport.getInstance().wire(this);
        Auditable auditable = (Auditable) object;
        auditable.setCreatedBy(userProvider.getCurrentUser());
        auditable.setCreatedDate(dateProvider.getCurrentDate());
    }

    @PreUpdate
    public void preUpdate(Object object) {
        AuditableEntityListenerWiringSupport.getInstance().wire(this);
        Auditable auditable = (Auditable) object;
        auditable.setModifiedBy(userProvider.getCurrentUser());
        auditable.setModifiedDate(dateProvider.getCurrentDate());
    }

    public void wire(UserProvider userProvider, DateProvider dateProvider) {
        this.userProvider = userProvider;
        this.dateProvider = dateProvider;
    }
}
