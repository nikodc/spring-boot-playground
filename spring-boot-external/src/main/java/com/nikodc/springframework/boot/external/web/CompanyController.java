package com.nikodc.springframework.boot.external.web;

import com.nikodc.springframework.boot.external.company.domain.Company;
import com.nikodc.springframework.boot.external.company.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    public Company findById(@PathVariable Long id) {
        Company company = companyService.findById(id);
        return company;
    }

}
