package com.nikodc.springframework.boot.external.company.service;

import com.nikodc.springframework.boot.external.company.domain.Company;
import com.nikodc.springframework.boot.external.company.domain.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Transactional(readOnly = true)
    public Company findById(Long id) {
        Company company = companyRepository.findOne(id);
        return company;
    }

}
