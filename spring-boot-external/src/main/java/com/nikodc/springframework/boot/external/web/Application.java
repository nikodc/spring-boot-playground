package com.nikodc.springframework.boot.external.web;

import com.nikodc.springframework.boot.core.Dual;
import com.nikodc.springframework.boot.core.DualRepository;
import com.nikodc.springframework.boot.external.company.domain.Company;
import com.nikodc.springframework.boot.external.company.domain.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan("com.nikodc.springframework.boot")
@EnableJpaRepositories(basePackages = {"com.nikodc.springframework.boot"})
@EntityScan(basePackages = {"com.nikodc.springframework.boot"})
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

    @Autowired
    private DualRepository dualRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

        // init dual table
        dualRepository.save(new Dual("X"));

        // save a couple of companies
        companyRepository.save(new Company("MultiAhorro", "ds1"));
        companyRepository.save(new Company("El Dorado", "ds2"));
        companyRepository.save(new Company("Super Frigo", "ds3"));
        companyRepository.save(new Company("Granja La Familia", "ds4"));
        companyRepository.save(new Company("Maratón", "ds5"));

        // fetch all companies
        System.out.println("Companies found with findAll():");
        System.out.println("-------------------------------");
        for (Company company : companyRepository.findAll()) {
            System.out.println(company);
        }
        System.out.println();

        // fetch an individual company by ID
        Company company = companyRepository.findOne(1L);
        System.out.println("Company found with findOne(1L):");
        System.out.println("--------------------------------");
        System.out.println(company);
        System.out.println();

    }

}
