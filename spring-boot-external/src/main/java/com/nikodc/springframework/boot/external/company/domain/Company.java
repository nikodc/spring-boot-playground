package com.nikodc.springframework.boot.external.company.domain;

import com.nikodc.springframework.boot.audit.domain.Auditable;

import javax.persistence.*;
import javax.persistence.Entity;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "companies")
public class Company extends Auditable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_companies_id")
    @SequenceGenerator(name = "seq_companies_id", sequenceName = "seq_companies_id", allocationSize = 1)
    private Long id;

    @Column(name = "name", length = 255)
    private String name;

    @Column(name = "datasource", length = 20)
    private String datasource;

    public Company() {
        super();
    }

    public Company(String name, String datasource) {
        this.name = name;
        this.datasource = datasource;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return String.format(
                "Company[id=%d, name='%s', datasource='%s', " +
                        "createdBy='%s', createdDate=%s, modifiedBy='%s', modifiedDate=%s]",
                id, name, datasource, createdBy,
                createdDate != null ? df.format(createdDate) : null,
                modifiedBy,
                modifiedDate != null ? df.format(modifiedDate) : null);
    }
}
