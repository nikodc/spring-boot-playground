package com.nikodc.springframework.boot.webapp.ping.domain;

import com.nikodc.springframework.boot.audit.domain.Auditable;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "ping_logs")
public class PingLog extends Auditable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ping_logs_id")
    @SequenceGenerator(name = "seq_ping_logs_id", sequenceName = "seq_ping_logs_id", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "company_id")
    private Long companyId;

    @Column(name = "name", length = 255)
    private String name;

    @Column(name = "datasource", length = 20)
    private String datasource;

    public PingLog() {
        super();
    }

    public PingLog(Long companyId, String name, String datasource) {
        this.companyId = companyId;
        this.name = name;
        this.datasource = datasource;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return String.format(
                "PingLog[id=%d, companyId=%d, name='%s', datasource='%s', " +
                        "createdBy='%s', createdDate=%s, modifiedBy='%s', modifiedDate=%s]",
                id, companyId, name, datasource, createdBy,
                createdDate != null ? df.format(createdDate) : null,
                modifiedBy,
                modifiedDate != null ? df.format(modifiedDate) : null);
    }
}
