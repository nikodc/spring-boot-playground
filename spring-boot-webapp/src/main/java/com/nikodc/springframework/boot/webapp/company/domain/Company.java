package com.nikodc.springframework.boot.webapp.company.domain;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Company {

    private Long id;

    private String name;

    private String datasource;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return String.format(
                "Company[id=%d, name='%s', datasource='%s']", id, name, datasource);
    }
}
