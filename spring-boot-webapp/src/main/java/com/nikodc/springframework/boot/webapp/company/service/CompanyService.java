package com.nikodc.springframework.boot.webapp.company.service;

import com.nikodc.springframework.boot.webapp.company.domain.Company;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CompanyService {

    public Company findById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        Company company = restTemplate.getForObject("http://spring-boot-external.herokuapp.com/companies/{id}",
                Company.class, id);
        return company;
    }

}
