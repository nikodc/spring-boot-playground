package com.nikodc.springframework.boot.webapp.ping.service;

import com.nikodc.springframework.boot.webapp.ping.domain.PingLog;
import com.nikodc.springframework.boot.webapp.ping.domain.PingLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PingLogService {

    @Autowired
    private PingLogRepository pingLogRepository;

    @Transactional
    public PingLog save(PingLog pingLog) {
        pingLog = pingLogRepository.save(pingLog);
        return pingLog;
    }

    @Transactional(readOnly = true)
    public List<PingLog> findAll() {
        List<PingLog> pingLogs = pingLogRepository.findAll(new Sort(Sort.Direction.DESC, "id"));
        return pingLogs;
    }
}
