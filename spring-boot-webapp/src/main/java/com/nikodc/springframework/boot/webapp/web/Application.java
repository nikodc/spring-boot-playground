package com.nikodc.springframework.boot.webapp.web;

import com.nikodc.springframework.boot.core.Dual;
import com.nikodc.springframework.boot.core.DualRepository;
import com.nikodc.springframework.boot.webapp.ping.domain.PingLog;
import com.nikodc.springframework.boot.webapp.ping.domain.PingLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan("com.nikodc.springframework.boot")
@EnableJpaRepositories(basePackages = {"com.nikodc.springframework.boot"})
@EntityScan(basePackages = {"com.nikodc.springframework.boot"})
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

    @Autowired
    private DualRepository dualRepository;

    @Autowired
    private PingLogRepository pingLogRepository;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

        // init dual table
        dualRepository.save(new Dual("X"));

        // save a couple of ping logs
        pingLogRepository.save(new PingLog(1l, "MultiAhorro", "ds1"));
        pingLogRepository.save(new PingLog(2l, "El Dorado", "ds2"));
        pingLogRepository.save(new PingLog(3l, "Super Frigo", "ds3"));
        pingLogRepository.save(new PingLog(4l, "Granja La Familia", "ds4"));
        pingLogRepository.save(new PingLog(5l, "Maratón", "ds5"));

        // fetch all customers
        System.out.println("Ping logs found with findAll():");
        System.out.println("-------------------------------");
        for (PingLog pingLog : pingLogRepository.findAll()) {
            System.out.println(pingLog);
        }
        System.out.println();

        // fetch an individual ping log by ID
        PingLog pingLog = pingLogRepository.findOne(1L);
        System.out.println("Ping log found with findOne(1L):");
        System.out.println("--------------------------------");
        System.out.println(pingLog);
        System.out.println();

    }

}