package com.nikodc.springframework.boot.webapp.ping.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PingLogRepository extends JpaRepository<PingLog, Long> {

}
