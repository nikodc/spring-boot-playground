package com.nikodc.springframework.boot.webapp.web;

import com.nikodc.springframework.boot.webapp.company.domain.Company;
import com.nikodc.springframework.boot.webapp.company.service.CompanyService;
import com.nikodc.springframework.boot.webapp.ping.domain.PingLog;
import com.nikodc.springframework.boot.webapp.ping.service.PingLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping(value="/ping")
public class PingController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private PingLogService pingLogService;

    @RequestMapping(method= RequestMethod.GET)
    public List<PingLog> findAll() {
        List<PingLog> pingLogs = pingLogService.findAll();
        return pingLogs;
    }


    @RequestMapping(method= RequestMethod.POST)
    public PingLog ping(@PathParam("companyId") Long companyId) {
        Company company = companyService.findById(companyId);

        String name = null, datasource = null;
        if (company != null) {
            name = company.getName();
            datasource = company.getDatasource();
        }

        PingLog pingLog = pingLogService.save(new PingLog(companyId, name, datasource));

        return pingLog;
    }

}
