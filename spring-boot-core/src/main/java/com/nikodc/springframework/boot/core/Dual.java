package com.nikodc.springframework.boot.core;

import javax.persistence.*;

@Entity
@Table(name = "dual")
public class Dual extends DomainEntity {

    public Dual() {
        super();
    }

    public Dual(String dummy) {
        super();
        this.dummy = dummy;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_dual_id")
    @SequenceGenerator(name = "seq_dual_id", sequenceName = "seq_dual_id", allocationSize = 1)
    private Long id;

    @Column(name = "dummy", length = 1)
    private String dummy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDummy() {
        return dummy;
    }

    public void setDummy(String dummy) {
        this.dummy = dummy;
    }

}
